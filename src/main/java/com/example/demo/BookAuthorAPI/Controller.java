package com.example.demo.BookAuthorAPI;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping("/book")
    public ArrayList<Book> getListBook(){
        ArrayList<Book> books = new ArrayList<>();

        Author author1 = new Author("Ngoc" , 'F' ,"hellowword@yahoo.com");
        Author author2 = new Author("Hoa" , 'F' , "hihi@gmail.com");
        Author author3 = new Author("jisoo" , 'M' , "blackpink@facebook.com");
        
        Book book1 = new Book("Jennie" , author1,3.00,52321);
        Book book2 = new Book("Lisa" , author2,112.00,529);
        Book book3 = new Book("Rosie" , author3,42.00,2880);  
        
        books.add(book1);
        books.add(book2);
        books.add(book3);

        return books ;
    }

   
}
